(function (win) {
	'use strict';
	var Mplayer = function(opt){
		if (!opt) {
			throw Error('option needed.');
		}
		if ( !(this instanceof Mplayer) ) {
			return new Mplayer(opt);
		}
		this.init(opt);
		return this;
	};
	Mplayer.prototype = {
		constructor: Mplayer,
		init: function (opt) {
			this.opt = this.extend({autoplay: false}, opt);
			if (this.support.audio) {
				this.player = this._createAudioPlayer(opt);
			} else if (this.support.ActiveXObject) {
				this.player = this._createWMP(opt);
			} else {
				// :todo plugin support
			}
			this.playerList = [];
		},
		_createWMP: function(opt) {
			var _html = '<object height="0" width="0" ';
			var videoUrl = encodeURI(opt.src);
			var player = null;
			if (this.support.ie) {
				_html += 'classid="clsid:6BF52A52-394A-11d3-B153-00C04F79FAA6" >';
				_html += '<param name="url" value="' + videoUrl + '" />';
				_html += '<param name="enableContextMenu" value="0" />';
				_html += '<param name="uiMode " value="invisible" />';
			} else {
				_html += 'type="video/x-ms-wmp" ';
				_html += 'data="' + videoUrl + '" >';
			}
			_html += '<param name="autoStart" value="'+ +!!opt.autoplay +'" />';
			_html += '<param name="playCount" value="1" />';
			_html += '</object>';
			document.body.insertAdjacentHTML('beforeend', _html);

			player = document.body.lastChild;
			player.play = function () {
				return player.controls.play();
			};
			player.pause = function () {
				return player.controls.pause();
			};
			player.getCurrentTime = function () {
				return play.currentPositionTimecode;
			};
			player.src = player.URL || player.SRC;
			return player;
		},
		_createAudioPlayer: function(opt) {
			var au = document.createElement('audio');
			au.autoplay = opt.autoplay;
			au.src = encodeURI(opt.src);
			return au;
		},
		extend: function () {
			var options, name, src, copy, copyIsArray, clone,
				target = arguments[0] || {},
				i = 1,
				length = arguments.length,
				deep = false;
			if ( typeof target === "boolean" ) {
				deep = target;
				target = arguments[ i ] || {};
				i++;
			}
			if ( typeof target !== "object" && !jQuery.isFunction(target) ) {
				target = {};
			}
			if ( i === length ) {
				target = this;
				i--;
			}
			for ( ; i < length; i++ ) {
				if ( (options = arguments[ i ]) != null ) {
					for ( name in options ) {
						src = target[ name ];
						copy = options[ name ];
						if ( target === copy ) {
							continue;
						}
						if ( deep && copy && ( jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)) ) ) {
							if ( copyIsArray ) {
								copyIsArray = false;
								clone = src && jQuery.isArray(src) ? src : [];

							} else {
								clone = src && jQuery.isPlainObject(src) ? src : {};
							}
							target[ name ] = jQuery.extend( deep, clone, copy );
						} else if ( copy !== undefined ) {
							target[ name ] = copy;
						}
					}
				}
			}
			return target;
		},
		support: {
			audio: (function () {
				var au = document.createElement('audio');
				var rt = !!au.play;
				au = null;
				return rt;
			}()),
			ActiveXObject: !!(window.ActiveXObject || window.GeckoActiveXObject),
			ie: navigator.userAgent.indexOf('MSIE') >= 0
		}
	};

	win.Mplayer = Mplayer;
}(this));